FROM openjdk:11-jre-slim

# Install System Packages
RUN apt update && \
    #apt -y install --no-install-recommends build-essential  librdkafka-dev libyajl-dev npm jq \
    apt install telnet -y \ 
    && rm -rf /var/lib/apt/lists/*

# Install Kafka
ARG kafka_version=3.0.0
ARG scala_version=2.13

ENV KAFKA_VERSION=$kafka_version \
    SCALA_VERSION=$scala_version \
    KAFKA_HOME=/opt/kafka

ENV PATH=${PATH}:${KAFKA_HOME}/bin

ADD https://archive.apache.org/dist/kafka/${KAFKA_VERSION}/kafka_${SCALA_VERSION}-${KAFKA_VERSION}.tgz /tmp

RUN tar -xvf /tmp/kafka_${SCALA_VERSION}-${KAFKA_VERSION}.tgz -C /opt \
    && rm /tmp/kafka_${SCALA_VERSION}-${KAFKA_VERSION}.tgz \
    && ln -s /opt/kafka_${SCALA_VERSION}-${KAFKA_VERSION} ${KAFKA_HOME}

CMD ["bash", "-c", "while true; do sleep 86400; done"]
